public with sharing class Sec4Practice {
  // 1. Создать в апекс 5  контактов с любыми именами.Сохранить их в БД.
  public void createContacts() {
    List<Contact> contacts = new List<Contact>();
    for (integer i = 0; i < 5; i++) {
      Contact con = new Contact(
        FirstName = 'testFirstName' + i,
        LastName = 'testLastName' + i
      );
      contacts.add(con);
    }
    insert contacts;
  }

  // 2. Создать ещё 5 записей контактов с любымиименами и Title = “Developer”.
  // Сделать цикл который проходит по предыдущим контактам и так же сделать для них Title = “Developer”.
  // Одной операцией обновить/сохранить их в БД.
  public void createAndUpdateContacts() {
    List<Contact> contacts = [
      SELECT id, FirstName, LastName
      FROM Contact
      WHERE FirstName LIKE 'testFirstName%'
    ];
    for (Integer i = 0; i < contacts.size(); i++) {
      contacts.get(i).Title = 'Developer';
    }

    for (integer i = 0; i < 5; i++) {
      Contact con = new Contact(
        FirstName = 'testFirstName_' + i,
        LastName = 'testLastName_' + i,
        Title = 'Developer'
      );
      contacts.add(con);
    }
    upsert contacts;
  }

  //   3.   Удалить все 10 созданных контактов.
  public void deleteContacts() {
    List<Contact> contacts = [
      SELECT id, FirstName, LastName
      FROM Contact
      WHERE FirstName LIKE 'testFirstName%'
    ];
    delete contacts;
  }

  //   4.   Вытащить из корзины удалённые контакты.
  public void unDeleteContacts() {
    List<Contact> contacts = [
      SELECT id, FirstName, LastName
      FROM Contact
      WHERE FirstName LIKE 'testFirstName%'
      ALL ROWS
    ];
    undelete contacts;
  }

  //   5.   Сделать цикл, который создаёт 10 контактов с заполненным LastName и 10 контактов с пустым LastName. При помощи Database класса:
  //   а) сделать инсерт, который позволит частично сохранить полученный лист контактов
  //   б) После инсерта проверит количество сохранённых контактов
  //   в) если количество контактов в списке будет больше чем количество сохранённых
  //   контактов – сделает откат до момента инсерта.
  public void testDataBaseClass() {
    Integer countInBegin = [SELECT COUNT() FROM contact];
    System.debug(countInBegin);
    List<Contact> contacts = new List<Contact>();
    for (integer i = 0; i < 10; i++) {
      Contact con = new Contact(
        FirstName = 'testFirstName__' + i,
        LastName = 'testLastName__' + i
      );
      Contact con1 = new Contact(FirstName = 'testFirstName__' + i);
      contacts.add(con);
      contacts.add(con1);
    }
    Savepoint sp = Database.setSavepoint();
    Database.insert(contacts, false);
    Integer countInEnd = [SELECT COUNT() FROM contact];
    System.debug(countInEnd);

    if (countInEnd > countInBegin) {
      Database.rollback(sp);
    }
  }
}
