public with sharing class UserPageController {
  public String userFirstName { get; set; }
  //   public String userLastName = UserInfo.getLastName();
  public String userLastName { get; set; }
  public String userEmail { get; set; }
  public String orgName { get; set; }
  public ID userID { get; set; }
  public String cur { get; set; }
  public Date todayDate { get; set; }
  public Integer limitQueries { get; set; }

  //   public String getUserLastName() {
  //     return userLastName;
  //   }

  //   public void setUserLastName(String s){
  //     userLastName = s;
  //   }

  public UserPageController() {
    userFirstName = UserInfo.getFirstName();
    userLastName = UserInfo.getLastName();
    userEmail = UserInfo.getUserEmail();
    orgName = UserInfo.getOrganizationName();
    userID = UserInfo.getUserId();
    cur = UserInfo.getDefaultCurrency();
    todayDate = System.today();
    limitQueries = Limits.getLimitQueries();
  }
}
