public with sharing class CarEngineClass {
  public static void deleteCars() {
    List<Car__c> cars = [SELECT id FROM Car__c];
    delete cars;
  }

  public static void deleteEngines() {
    List<Engine__c> engines = [SELECT id FROM Engine__c];
    delete engines;
  }

  public static void deleteCarEngines() {
    List<CarEngine__c> carEngines = [SELECT id FROM CarEngine__c];
    delete carEngines;
  }

  public static void deleteAllData() {
    deleteCars();
    deleteEngines();
    deleteCarEngines();
  }

  // create engines
  public static void createEngines() {
    List<Engine__c> engines = new List<Engine__c>();
    for (integer i = 0; i < 100; i++) {
      Engine__c engine = new Engine__c(
        Engine_name__c = 'Engine name ' + i,
        Power__c = 1 + (0.0 + i) / 10
      );
      engines.add(engine);
    }

    insert engines;
  }

  // create cars
  public static void createCars() {
    List<Car__c> cars = new List<Car__c>();

    for (integer i = 0; i < 100; i++) {
      Car__c car = new Car__c(Car_Name__c = 'Car Name ' + i);
      cars.add(car);
    }

    insert cars;
  }

  // create carEngines
  public static void createCarEngines() {
    List<CarEngine__c> carEngines = new List<CarEngine__c>();
    List<Engine__c> engines = [SELECT id FROM Engine__c];
    List<Car__c> cars = [SELECT id FROM Car__c];

    for (integer i = 0; i < 150; i++) {
      Integer rand1 = Integer.valueOf(Math.random() * (cars.size() - 1));
      Integer rand2 = Integer.valueOf(Math.random() * (engines.size() - 1));
      CarEngine__c carEngine = new CarEngine__c(
        Title__c = 'carEngine ' + i,
        Car__c = cars.get(rand1).ID,
        Engine__c = engines.get(rand2).ID
      );
      carEngines.add(carEngine);
    }

    insert carEngines;
  }

  // 3. Получить в Лист все Двигатели.
  public static List<Engine__c> getAllEngine() {
    List<Engine__c> engines = [
      SELECT Id, Engine_name__c, Power__c, Name
      FROM Engine__c
    ];
    return engines;
  }

  //   4. Получить в Листы все Автомобили для каждого двигателя.
  public static Map<Engine__c, List<Car__c>> getEnginesAndCars() {
    Map<Engine__c, List<Car__c>> enginesAndCarsMap = new Map<Engine__c, List<Car__c>>();
    List<Car__c> cars = [SELECT id, Car_Name__c FROM Car__c];

    List<Engine__c> engines = [
      SELECT
        Id,
        Name,
        Engine_name__c,
        Power__c,
        (SELECT Car__c FROM CarEngines__r)
      FROM Engine__c
    ];

    for (Engine__c eng : engines) {
      List<Car__c> carsForEngine = new List<Car__c>();

      for (CarEngine__C carEng : eng.CarEngines__r) {
        for (Car__c car : cars) {
          if (carEng.Car__c == car.ID) {
            carsForEngine.add(car);
          }
        }
      }

      enginesAndCarsMap.put(eng, carsForEngine);
      system.debug(eng.Name + '        ---  ' + carsForEngine);
    }
    return enginesAndCarsMap;
  }

  // 5. Создать Мапу где ключ это Id автомобиля и значения это все детали автомобиля.
  public static Map<ID, List<CarEngine__C>> getCarsAnd_CarEngine() {
    List<Car__c> cars = [
      SELECT ID, (SELECT Id, Title__c FROM CarEngines__r)
      FROM Car__c
    ];
    Map<ID, List<CarEngine__C>> carsAnd_CarEngineMap = new Map<ID, List<CarEngine__C>>();
    for (Car__c car : cars) {
      carsAnd_CarEngineMap.put(car.ID, car.CarEngines__r);
      system.debug(
        ' Car ID =   ' +
        car.ID +
        '      CarEngines list - ' +
        car.CarEngines__r
      );
    }

    return carsAnd_CarEngineMap;
  }

  // 6. Создать Мапу где ключ это название двигателя, а значения это названия всех Автомобилей.
  public static Map<String, List<String>> getEngineNamesAndCarNames() {
    Map<String, List<String>> engineNamesAndCarNamesMap = new Map<String, List<String>>();

    List<CarEngine__c> carEngineList = [
      SELECT Id, Car__c, Car__r.Car_Name__c, Engine__c, Engine__r.Engine_Name__c
      FROM CarEngine__c
    ];

    List<Engine__c> engines = [
      SELECT Id, Name, Engine_name__c, Power__c
      FROM Engine__c
    ];
    for (Engine__c engine : engines) {
      engineNamesAndCarNamesMap.put(engine.Engine_name__c, new List<String>());
    }

    for (CarEngine__c carEngine : carEngineList) {
      engineNamesAndCarNamesMap.get(carEngine.Engine__r.Engine_Name__c)
        .add(carEngine.Car__r.Car_Name__c);
    }

    for (String key : engineNamesAndCarNamesMap.keySet()) {
      system.debug(
        '  ' +
        key +
        '        ---  ' +
        engineNamesAndCarNamesMap.get(key)
      );
    }
    return engineNamesAndCarNamesMap;
  }

  List<Account> ac = [SELECT id FROM Account];

  //   7. Склонировать все данные в БД так, чтоб каждая запись повторялась(т.к.будет по два одинаковых двигателя, автомобиля и т.п.)

  public static void cloneData() {
    List<Car__c> cars = [
      SELECT Id, Name, Car_Name__c, Year_of_production__c
      FROM Car__c
    ];
    List<Car__c> cars2 = cars.deepClone();
    insert cars2;

    List<Engine__c> engines = [
      SELECT Id, Name, Engine_name__c, Power__c
      FROM Engine__c
    ];
    List<Engine__c> engines2 = engines.deepClone();
    insert engines2;

    List<CarEngine__c> carEngines = [
      SELECT Id, Name, Title__c, Car__c, Engine__c
      FROM CarEngine__c
    ];
    List<CarEngine__c> carEngines2 = carEngines.deepClone();
    insert carEngines2;
  }

  // 8. Удалить все дубликаты с помощью повторной выборки данных из объектов ипутем их перебора. Используйте Мапу для сбора уникальных записей в БД.
  public static void deleteDuplicateData() {
    // delete duplicate cars
    List<Car__c> cars = [
      SELECT Id, Name, Car_Name__c, Year_of_production__c
      FROM Car__c
    ];
    Map<String, ID> carsMap = new Map<String, ID>();
    for (Car__c car : cars) {
      if (carsMap.containsKey(car.Car_Name__c)) {
        continue;
      }
      carsMap.put(car.Car_Name__c, car.ID);
    }
    List<Car__c> carsToDelete = [
      SELECT Id, Name, Car_Name__c
      FROM Car__c
      WHERE ID NOT IN :carsMap.Values()
    ];
    delete carsToDelete;

    // delete duplicate engines
    List<Engine__c> engines = [
      SELECT Id, Name, Engine_name__c, Power__c
      FROM Engine__c
    ];
    Map<String, ID> enginesMap = new Map<String, ID>();
    for (Engine__c engine : engines) {
      if (enginesMap.containsKey(engine.Engine_name__c)) {
        continue;
      }
      enginesMap.put(engine.Engine_name__c, engine.ID);
    }
    List<Engine__c> enginesToDelete = [
      SELECT Id, Name, Engine_name__c, Power__c
      FROM Engine__c
      WHERE ID NOT IN :enginesMap.Values()
    ];
    delete enginesToDelete;

    // delete duplicate carEngines
    List<CarEngine__c> carEngines = [
      SELECT Id, Name, Title__c, Car__c, Engine__c
      FROM CarEngine__c
    ];
    Map<String, ID> carEnginesMap = new Map<String, ID>();
    for (CarEngine__c carEngine : carEngines) {
      carEnginesMap.put(carEngine.Title__c, carEngine.ID);
    }
    List<CarEngine__c> carEnginesToDelete = [
      SELECT Id, Name, Title__c, Car__c, Engine__c
      FROM CarEngine__c
      WHERE ID NOT IN :carEnginesMap.Values()
    ];
    delete carEnginesToDelete;
  }
}
